#include "pi_sqlite_SQLiteStatement.h"
#include "sqlite3.h"
#include <stdio.h>
#include "Common.h"
#include <assert.h>
#include <string.h>
#include <android/log.h>

static sqlite3_stmt* GetStmt(JNIEnv *env, jobject self)
{
	jclass klass = env->GetObjectClass(self);
	jfieldID fid = env->GetFieldID(klass, "mNativePtr", "J");
	assert(fid != NULL);
    jlong ptr = env->GetLongField(self, fid);
    env->DeleteLocalRef(klass);
    return (sqlite3_stmt*)(ptr);
}

static SqliteDB* GetDB(JNIEnv *env, jobject self)
{
	jclass klass = env->GetObjectClass(self);
	jfieldID fid = env->GetFieldID(klass, "mDbNativePtr", "J");
	assert(fid != NULL);
    jlong ptr = env->GetLongField(self, fid);
    env->DeleteLocalRef(klass);
    return (SqliteDB*)(ptr);
}

jint JNICALL Java_pi_sqlite_SQLiteStatement_prepare
(JNIEnv * env, jobject self, jstring jstrSQL)
{
	jclass klass = env->GetObjectClass(self);
	jfieldID fid = env->GetFieldID(klass, "mDbNativePtr", "J");
	assert(fid != NULL);
    jlong ptr = env->GetLongField(self, fid);
    SqliteDB* db = (SqliteDB*)ptr;
    assert(db != NULL);

	fid = env->GetFieldID(klass, "mNativePtr", "J");
	assert(fid != NULL);
	ptr = env->GetLongField(self, fid);
    sqlite3_stmt *stmt = (sqlite3_stmt*)ptr;
    if (stmt != NULL)
    {
    	sqlite3_finalize(stmt);
    	stmt = NULL;
    	env->SetLongField(self, fid, 0);
    }
   
	const char* sql = env->GetStringUTFChars(jstrSQL, NULL);
	int sqlLen = env->GetStringUTFLength(jstrSQL);
	int ret = sqlite3_prepare_v2(db->conn, sql, sqlLen, &stmt, NULL);
	env->ReleaseStringUTFChars(jstrSQL, sql);

	env->SetLongField(self, fid, (jlong)stmt);
	env->DeleteLocalRef(klass);

	return ret;
}


void JNICALL Java_pi_sqlite_SQLiteStatement_reset
(JNIEnv *env, jobject self)
{
	sqlite3_stmt* stmt = GetStmt(env, self);
	if (stmt != NULL) 
	{
		sqlite3_reset(stmt);
	}
}

void JNICALL Java_pi_sqlite_SQLiteStatement_close
(JNIEnv *env, jobject self)
{
	jclass klass = env->GetObjectClass(self);
	jfieldID fid = env->GetFieldID(klass, "mNativePtr", "J");
	assert(fid != NULL);
    jlong ptr = env->GetLongField(self, fid);
    
    sqlite3_stmt* stmt = (sqlite3_stmt*)(ptr);

    if (stmt != NULL) 
    {
    	env->SetLongField(self, fid, 0);
    	sqlite3_finalize(stmt);
    	stmt = NULL;	
    }

	env->DeleteLocalRef(klass);
}

jint JNICALL Java_pi_sqlite_SQLiteStatement_step
(JNIEnv * env, jobject self)
{
	sqlite3_stmt* stmt = GetStmt(env, self);
	if (stmt == NULL)
	{
		return -1;
	}
	return sqlite3_step(stmt);
}


jint JNICALL Java_pi_sqlite_SQLiteStatement_bind__I_3BII
(JNIEnv *env, jobject self, jint index, jbyteArray jBlob, jint offset, jint size)
{
	sqlite3_stmt* stmt = GetStmt(env, self);
	assert(stmt != NULL);
	assert(jBlob != NULL);
	assert(offset >= 0);
	assert(size >= 0);

	jint len = env->GetArrayLength(jBlob);
	if (len == 0 || size == 0) {
		return SQLITE_OK;
	}

	int realSize = size;
	if (realSize + offset > len) {
		realSize = len - offset;
	}
	void* buffer = malloc(realSize);
	env->GetByteArrayRegion(jBlob, offset, realSize, (jbyte *)buffer);

	return sqlite3_bind_blob(stmt, index, buffer, realSize, free);
}	


jint JNICALL Java_pi_sqlite_SQLiteStatement_bind__ID
(JNIEnv *env, jobject self, jint index, jdouble v)
{
	sqlite3_stmt* stmt = GetStmt(env, self);
	if (stmt == NULL)
	{
		return -1;
	}

	return sqlite3_bind_double(stmt, index, v);
}

jint JNICALL Java_pi_sqlite_SQLiteStatement_bind__II
(JNIEnv *env, jobject self, jint index, jint v)
{
	sqlite3_stmt* stmt = GetStmt(env, self);
	if (stmt == NULL)
	{
		return -1;
	}
	return sqlite3_bind_int(stmt, index, v);
}


jint JNICALL Java_pi_sqlite_SQLiteStatement_bind__IJ
(JNIEnv * env, jobject self, jint index, jlong v)
{
	sqlite3_stmt* stmt = GetStmt(env, self);
	if (stmt ==NULL)
	{
		return -1;
	}
	return sqlite3_bind_int64(stmt, index, v);
}

jint JNICALL Java_pi_sqlite_SQLiteStatement_bind__ILjava_lang_String_2
(JNIEnv *env, jobject self, jint index, jstring jstrValue)
{
	sqlite3_stmt* stmt = GetStmt(env, self);
	if (stmt == NULL)
	{
		return -1;
	}
	
	if (jstrValue == NULL) 
	{
		return sqlite3_bind_null(stmt, index);
	}

	int bytes = env->GetStringUTFLength(jstrValue);
	if (bytes <= 0)
	{
		return sqlite3_bind_null(stmt, index);
	}

	char* v = (char*)malloc(bytes + 1);

	int len = env->GetStringLength(jstrValue);
	env->GetStringUTFRegion(jstrValue, 0, len, v);

	return sqlite3_bind_text(stmt, index, v, bytes, free);
}


jint JNICALL Java_pi_sqlite_SQLiteStatement_bindNull
(JNIEnv *env, jobject self, jint index)
{
	sqlite3_stmt* stmt = GetStmt(env, self);
	if (stmt == NULL)
	{
		return -1;
	}
	sqlite3_bind_null(stmt, index);
}

jint JNICALL Java_pi_sqlite_SQLiteStatement_bindZeroBlob
(JNIEnv *env, jobject self, jint index, jint size)
{
	sqlite3_stmt* stmt = GetStmt(env, self);
	if (stmt == NULL)
	{
		return -1;
	}
	sqlite3_bind_zeroblob(stmt, index, 0);
}

jint JNICALL Java_pi_sqlite_SQLiteStatement_getColumnCount
(JNIEnv *env, jobject self)
{
	sqlite3_stmt* stmt = GetStmt(env, self);
	if (stmt == NULL)
	{
		return -1;
	}
	return sqlite3_column_count(stmt);
}

jstring JNICALL Java_pi_sqlite_SQLiteStatement_getColumnName
(JNIEnv *env, jobject self, jint index)
{
	sqlite3_stmt* stmt = GetStmt(env, self);
	if (stmt == NULL)
	{
		return NULL;
	}
	const char* name = sqlite3_column_name(stmt, index);
	return env->NewStringUTF(name);
}

jint JNICALL Java_pi_sqlite_SQLiteStatement_getColumnType
(JNIEnv *env, jobject self, jint index)
{
	sqlite3_stmt* stmt = GetStmt(env, self);
	if (stmt == NULL)
	{
		return -1;
	}
	return sqlite3_column_type(stmt, index);
}

jbyteArray JNICALL Java_pi_sqlite_SQLiteStatement_getColumnBlob
(JNIEnv *env, jobject self, jint index)
{
	sqlite3_stmt* stmt = GetStmt(env, self);
	if (stmt == NULL)
	{
		return NULL;
	}

	const void* blob = sqlite3_column_blob(stmt, index);
	int size = sqlite3_column_bytes(stmt, index);
	if (blob == NULL || size <= 0) {
		return NULL;
	} else {
		jbyteArray ret = env->NewByteArray(size);
		env->SetByteArrayRegion(ret, 0, size, (jbyte*)blob);
		return ret;
	}
}


jdouble JNICALL Java_pi_sqlite_SQLiteStatement_getColumnDouble
(JNIEnv *env, jobject self, jint index)
{
	sqlite3_stmt* stmt = GetStmt(env, self);
	if (stmt == NULL)
	{
		return 0;
	}
	return sqlite3_column_double(stmt, index);
}

jint JNICALL Java_pi_sqlite_SQLiteStatement_getColumnInt
(JNIEnv *env, jobject self, jint index)
{
	sqlite3_stmt* stmt = GetStmt(env, self);
	if (stmt == NULL)
	{
		return 0;
	}
	return sqlite3_column_int(stmt, index);
}

jlong JNICALL Java_pi_sqlite_SQLiteStatement_getColumnLong
(JNIEnv *env, jobject self, jint index)
{
	sqlite3_stmt* stmt = GetStmt(env, self);
	if (stmt == NULL)
	{
		return 0;
	}
	return sqlite3_column_int64(stmt, index);
}


jstring JNICALL Java_pi_sqlite_SQLiteStatement_getColumnText
(JNIEnv *env, jobject self, jint index)
{
	sqlite3_stmt* stmt = GetStmt(env, self);
	if (stmt == NULL)
	{
		return NULL;
	}
	const unsigned char* ret = sqlite3_column_text(stmt, index);
	return env->NewStringUTF((const char*)ret);
}

JNIEXPORT jint JNICALL Java_pi_sqlite_SQLiteStatement_getChanges
(JNIEnv * env, jobject self)
{
	SqliteDB* db = GetDB(env, self);
	return sqlite3_changes(db->conn);
}

JNIEXPORT jint JNICALL Java_pi_sqlite_SQLiteStatement_getInsertId
(JNIEnv * env, jobject self)
{
	SqliteDB* db = GetDB(env, self);
	return sqlite3_last_insert_rowid(db->conn);
}










