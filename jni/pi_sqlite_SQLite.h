/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class pi_sqlite_SQLite */

#ifndef _Included_pi_sqlite_SQLite
#define _Included_pi_sqlite_SQLite
#ifdef __cplusplus
extern "C" {
#endif
#undef pi_sqlite_SQLite_OPEN_READONLY
#define pi_sqlite_SQLite_OPEN_READONLY 1L
#undef pi_sqlite_SQLite_OPEN_READWRITE
#define pi_sqlite_SQLite_OPEN_READWRITE 2L
#undef pi_sqlite_SQLite_OPEN_CREATE
#define pi_sqlite_SQLite_OPEN_CREATE 4L
#undef pi_sqlite_SQLite_OPEN_DELETEONCLOSE
#define pi_sqlite_SQLite_OPEN_DELETEONCLOSE 8L
#undef pi_sqlite_SQLite_OPEN_EXCLUSIVE
#define pi_sqlite_SQLite_OPEN_EXCLUSIVE 16L
#undef pi_sqlite_SQLite_OPEN_AUTOPROXY
#define pi_sqlite_SQLite_OPEN_AUTOPROXY 32L
#undef pi_sqlite_SQLite_OPEN_URI
#define pi_sqlite_SQLite_OPEN_URI 64L
#undef pi_sqlite_SQLite_OPEN_MEMORY
#define pi_sqlite_SQLite_OPEN_MEMORY 128L
#undef pi_sqlite_SQLite_OPEN_MAIN_DB
#define pi_sqlite_SQLite_OPEN_MAIN_DB 256L
#undef pi_sqlite_SQLite_OPEN_TEMP_DB
#define pi_sqlite_SQLite_OPEN_TEMP_DB 512L
#undef pi_sqlite_SQLite_OPEN_TRANSIENT_DB
#define pi_sqlite_SQLite_OPEN_TRANSIENT_DB 1024L
#undef pi_sqlite_SQLite_OPEN_MAIN_JOURNAL
#define pi_sqlite_SQLite_OPEN_MAIN_JOURNAL 2048L
#undef pi_sqlite_SQLite_OPEN_TEMP_JOURNAL
#define pi_sqlite_SQLite_OPEN_TEMP_JOURNAL 4096L
#undef pi_sqlite_SQLite_OPEN_SUBJOURNAL
#define pi_sqlite_SQLite_OPEN_SUBJOURNAL 8192L
#undef pi_sqlite_SQLite_OPEN_MASTER_JOURNAL
#define pi_sqlite_SQLite_OPEN_MASTER_JOURNAL 16384L
#undef pi_sqlite_SQLite_OPEN_NOMUTEX
#define pi_sqlite_SQLite_OPEN_NOMUTEX 32768L
#undef pi_sqlite_SQLite_OPEN_FULLMUTEX
#define pi_sqlite_SQLite_OPEN_FULLMUTEX 65536L
#undef pi_sqlite_SQLite_OPEN_SHAREDCACHE
#define pi_sqlite_SQLite_OPEN_SHAREDCACHE 131072L
#undef pi_sqlite_SQLite_OPEN_PRIVATECACHE
#define pi_sqlite_SQLite_OPEN_PRIVATECACHE 262144L
#undef pi_sqlite_SQLite_OPEN_WAL
#define pi_sqlite_SQLite_OPEN_WAL 524288L
#undef pi_sqlite_SQLite_OK
#define pi_sqlite_SQLite_OK 0L
#undef pi_sqlite_SQLite_ERROR
#define pi_sqlite_SQLite_ERROR 1L
#undef pi_sqlite_SQLite_INTERNAL
#define pi_sqlite_SQLite_INTERNAL 2L
#undef pi_sqlite_SQLite_PERM
#define pi_sqlite_SQLite_PERM 3L
#undef pi_sqlite_SQLite_ABORT
#define pi_sqlite_SQLite_ABORT 4L
#undef pi_sqlite_SQLite_BUSY
#define pi_sqlite_SQLite_BUSY 5L
#undef pi_sqlite_SQLite_LOCKED
#define pi_sqlite_SQLite_LOCKED 6L
#undef pi_sqlite_SQLite_NOMEM
#define pi_sqlite_SQLite_NOMEM 7L
#undef pi_sqlite_SQLite_READONLY
#define pi_sqlite_SQLite_READONLY 8L
#undef pi_sqlite_SQLite_INTERRUPT
#define pi_sqlite_SQLite_INTERRUPT 9L
#undef pi_sqlite_SQLite_IOERR
#define pi_sqlite_SQLite_IOERR 10L
#undef pi_sqlite_SQLite_CORRUPT
#define pi_sqlite_SQLite_CORRUPT 11L
#undef pi_sqlite_SQLite_NOTFOUND
#define pi_sqlite_SQLite_NOTFOUND 12L
#undef pi_sqlite_SQLite_FULL
#define pi_sqlite_SQLite_FULL 13L
#undef pi_sqlite_SQLite_CANTOPEN
#define pi_sqlite_SQLite_CANTOPEN 14L
#undef pi_sqlite_SQLite_PROTOCOL
#define pi_sqlite_SQLite_PROTOCOL 15L
#undef pi_sqlite_SQLite_EMPTY
#define pi_sqlite_SQLite_EMPTY 16L
#undef pi_sqlite_SQLite_SCHEMA
#define pi_sqlite_SQLite_SCHEMA 17L
#undef pi_sqlite_SQLite_TOOBIG
#define pi_sqlite_SQLite_TOOBIG 18L
#undef pi_sqlite_SQLite_CONSTRAINT
#define pi_sqlite_SQLite_CONSTRAINT 19L
#undef pi_sqlite_SQLite_MISMATCH
#define pi_sqlite_SQLite_MISMATCH 20L
#undef pi_sqlite_SQLite_MISUSE
#define pi_sqlite_SQLite_MISUSE 21L
#undef pi_sqlite_SQLite_NOLFS
#define pi_sqlite_SQLite_NOLFS 22L
#undef pi_sqlite_SQLite_AUTH
#define pi_sqlite_SQLite_AUTH 23L
#undef pi_sqlite_SQLite_FORMAT
#define pi_sqlite_SQLite_FORMAT 24L
#undef pi_sqlite_SQLite_RANGE
#define pi_sqlite_SQLite_RANGE 25L
#undef pi_sqlite_SQLite_NOTADB
#define pi_sqlite_SQLite_NOTADB 26L
#undef pi_sqlite_SQLite_NOTICE
#define pi_sqlite_SQLite_NOTICE 27L
#undef pi_sqlite_SQLite_WARNING
#define pi_sqlite_SQLite_WARNING 28L
#undef pi_sqlite_SQLite_ROW
#define pi_sqlite_SQLite_ROW 100L
#undef pi_sqlite_SQLite_DONE
#define pi_sqlite_SQLite_DONE 101L
#undef pi_sqlite_SQLite_IOERR_READ
#define pi_sqlite_SQLite_IOERR_READ 266L
#undef pi_sqlite_SQLite_IOERR_SHORT_READ
#define pi_sqlite_SQLite_IOERR_SHORT_READ 522L
#undef pi_sqlite_SQLite_IOERR_WRITE
#define pi_sqlite_SQLite_IOERR_WRITE 778L
#undef pi_sqlite_SQLite_IOERR_FSYNC
#define pi_sqlite_SQLite_IOERR_FSYNC 1034L
#undef pi_sqlite_SQLite_IOERR_DIR_FSYNC
#define pi_sqlite_SQLite_IOERR_DIR_FSYNC 1290L
#undef pi_sqlite_SQLite_IOERR_TRUNCATE
#define pi_sqlite_SQLite_IOERR_TRUNCATE 1546L
#undef pi_sqlite_SQLite_IOERR_FSTAT
#define pi_sqlite_SQLite_IOERR_FSTAT 1802L
#undef pi_sqlite_SQLite_IOERR_UNLOCK
#define pi_sqlite_SQLite_IOERR_UNLOCK 2058L
#undef pi_sqlite_SQLite_IOERR_RDLOCK
#define pi_sqlite_SQLite_IOERR_RDLOCK 2314L
#undef pi_sqlite_SQLite_IOERR_DELETE
#define pi_sqlite_SQLite_IOERR_DELETE 2570L
#undef pi_sqlite_SQLite_IOERR_BLOCKED
#define pi_sqlite_SQLite_IOERR_BLOCKED 2826L
#undef pi_sqlite_SQLite_IOERR_NOMEM
#define pi_sqlite_SQLite_IOERR_NOMEM 3082L
#undef pi_sqlite_SQLite_IOERR_ACCESS
#define pi_sqlite_SQLite_IOERR_ACCESS 3338L
#undef pi_sqlite_SQLite_IOERR_CHECKRESERVEDLOCK
#define pi_sqlite_SQLite_IOERR_CHECKRESERVEDLOCK 3594L
#undef pi_sqlite_SQLite_IOERR_LOCK
#define pi_sqlite_SQLite_IOERR_LOCK 3850L
#undef pi_sqlite_SQLite_IOERR_CLOSE
#define pi_sqlite_SQLite_IOERR_CLOSE 4106L
#undef pi_sqlite_SQLite_IOERR_DIR_CLOSE
#define pi_sqlite_SQLite_IOERR_DIR_CLOSE 4362L
#undef pi_sqlite_SQLite_IOERR_SHMOPEN
#define pi_sqlite_SQLite_IOERR_SHMOPEN 4618L
#undef pi_sqlite_SQLite_IOERR_SHMSIZE
#define pi_sqlite_SQLite_IOERR_SHMSIZE 4874L
#undef pi_sqlite_SQLite_IOERR_SHMLOCK
#define pi_sqlite_SQLite_IOERR_SHMLOCK 5130L
#undef pi_sqlite_SQLite_IOERR_SHMMAP
#define pi_sqlite_SQLite_IOERR_SHMMAP 5386L
#undef pi_sqlite_SQLite_IOERR_SEEK
#define pi_sqlite_SQLite_IOERR_SEEK 5642L
#undef pi_sqlite_SQLite_IOERR_DELETE_NOENT
#define pi_sqlite_SQLite_IOERR_DELETE_NOENT 5898L
#undef pi_sqlite_SQLite_IOERR_MMAP
#define pi_sqlite_SQLite_IOERR_MMAP 6154L
#undef pi_sqlite_SQLite_IOERR_GETTEMPPATH
#define pi_sqlite_SQLite_IOERR_GETTEMPPATH 6410L
#undef pi_sqlite_SQLite_IOERR_CONVPATH
#define pi_sqlite_SQLite_IOERR_CONVPATH 6666L
#undef pi_sqlite_SQLite_LOCKED_SHAREDCACHE
#define pi_sqlite_SQLite_LOCKED_SHAREDCACHE 262L
#undef pi_sqlite_SQLite_BUSY_RECOVERY
#define pi_sqlite_SQLite_BUSY_RECOVERY 261L
#undef pi_sqlite_SQLite_BUSY_SNAPSHOT
#define pi_sqlite_SQLite_BUSY_SNAPSHOT 517L
#undef pi_sqlite_SQLite_CANTOPEN_NOTEMPDIR
#define pi_sqlite_SQLite_CANTOPEN_NOTEMPDIR 270L
#undef pi_sqlite_SQLite_CANTOPEN_ISDIR
#define pi_sqlite_SQLite_CANTOPEN_ISDIR 526L
#undef pi_sqlite_SQLite_CANTOPEN_FULLPATH
#define pi_sqlite_SQLite_CANTOPEN_FULLPATH 782L
#undef pi_sqlite_SQLite_CANTOPEN_CONVPATH
#define pi_sqlite_SQLite_CANTOPEN_CONVPATH 1038L
#undef pi_sqlite_SQLite_CORRUPT_VTAB
#define pi_sqlite_SQLite_CORRUPT_VTAB 267L
#undef pi_sqlite_SQLite_READONLY_RECOVERY
#define pi_sqlite_SQLite_READONLY_RECOVERY 264L
#undef pi_sqlite_SQLite_READONLY_CANTLOCK
#define pi_sqlite_SQLite_READONLY_CANTLOCK 520L
#undef pi_sqlite_SQLite_READONLY_ROLLBACK
#define pi_sqlite_SQLite_READONLY_ROLLBACK 776L
#undef pi_sqlite_SQLite_READONLY_DBMOVED
#define pi_sqlite_SQLite_READONLY_DBMOVED 1032L
#undef pi_sqlite_SQLite_ABORT_ROLLBACK
#define pi_sqlite_SQLite_ABORT_ROLLBACK 516L
#undef pi_sqlite_SQLite_CONSTRAINT_CHECK
#define pi_sqlite_SQLite_CONSTRAINT_CHECK 275L
#undef pi_sqlite_SQLite_CONSTRAINT_COMMITHOOK
#define pi_sqlite_SQLite_CONSTRAINT_COMMITHOOK 531L
#undef pi_sqlite_SQLite_CONSTRAINT_FOREIGNKEY
#define pi_sqlite_SQLite_CONSTRAINT_FOREIGNKEY 787L
#undef pi_sqlite_SQLite_CONSTRAINT_FUNCTION
#define pi_sqlite_SQLite_CONSTRAINT_FUNCTION 1043L
#undef pi_sqlite_SQLite_CONSTRAINT_NOTNULL
#define pi_sqlite_SQLite_CONSTRAINT_NOTNULL 1299L
#undef pi_sqlite_SQLite_CONSTRAINT_PRIMARYKEY
#define pi_sqlite_SQLite_CONSTRAINT_PRIMARYKEY 1555L
#undef pi_sqlite_SQLite_CONSTRAINT_TRIGGER
#define pi_sqlite_SQLite_CONSTRAINT_TRIGGER 1811L
#undef pi_sqlite_SQLite_CONSTRAINT_UNIQUE
#define pi_sqlite_SQLite_CONSTRAINT_UNIQUE 2067L
#undef pi_sqlite_SQLite_CONSTRAINT_VTAB
#define pi_sqlite_SQLite_CONSTRAINT_VTAB 2323L
#undef pi_sqlite_SQLite_CONSTRAINT_ROWID
#define pi_sqlite_SQLite_CONSTRAINT_ROWID 2579L
#undef pi_sqlite_SQLite_NOTICE_RECOVER_WAL
#define pi_sqlite_SQLite_NOTICE_RECOVER_WAL 283L
#undef pi_sqlite_SQLite_NOTICE_RECOVER_ROLLBACK
#define pi_sqlite_SQLite_NOTICE_RECOVER_ROLLBACK 539L
#undef pi_sqlite_SQLite_WARNING_AUTOINDEX
#define pi_sqlite_SQLite_WARNING_AUTOINDEX 284L
/*
 * Class:     pi_sqlite_SQLite
 * Method:    open
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_pi_sqlite_SQLite_open__Ljava_lang_String_2
  (JNIEnv *, jobject, jstring);

/*
 * Class:     pi_sqlite_SQLite
 * Method:    open
 * Signature: (Ljava/lang/String;ILjava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_pi_sqlite_SQLite_open__Ljava_lang_String_2ILjava_lang_String_2
  (JNIEnv *, jobject, jstring, jint, jstring);

/*
 * Class:     pi_sqlite_SQLite
 * Method:    close
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_pi_sqlite_SQLite_close
  (JNIEnv *, jobject);

/*
 * Class:     pi_sqlite_SQLite
 * Method:    createStatement
 * Signature: ()Lpi/sqlite/SQLiteStatement;
 */
JNIEXPORT jobject JNICALL Java_pi_sqlite_SQLite_createStatement
  (JNIEnv *, jobject);

/*
 * Class:     pi_sqlite_SQLite
 * Method:    getErrorCode
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_pi_sqlite_SQLite_getErrorCode
  (JNIEnv *, jobject);

/*
 * Class:     pi_sqlite_SQLite
 * Method:    getExtendedErrorCode
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_pi_sqlite_SQLite_getExtendedErrorCode
  (JNIEnv *, jobject);

/*
 * Class:     pi_sqlite_SQLite
 * Method:    getErrorString
 * Signature: (I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_pi_sqlite_SQLite_getErrorString
  (JNIEnv *, jobject, jint);

/*
 * Class:     pi_sqlite_SQLite
 * Method:    getErrorMessage
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_pi_sqlite_SQLite_getErrorMessage
  (JNIEnv *, jobject);

#ifdef __cplusplus
}
#endif
#endif
