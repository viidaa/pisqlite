LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

SRC_DIR = $(LOCAL_PATH)

########################################################################################

LOCAL_SRC_FILES  := sqlite3.c \
					pi_sqlite_SQLite.cpp \
					pi_sqlite_SQLiteStatement.cpp 

LOCAL_MODULE     := pisqlite
LOCAL_LDLIBS     := -llog 

ifeq ($(NDK_DEBUG), 1)
    LOCAL_CFLAGS += -DDEBUG=1
endif

include $(BUILD_SHARED_LIBRARY)

