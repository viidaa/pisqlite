#include "pi_sqlite_SQLite.h"
#include "sqlite3.h"
#include "Common.h"
#include <assert.h>
#include <stdio.h>
#include <android/log.h>

static SqliteDB* GetDB(JNIEnv *env, jobject self)
{
	jclass klass = env->GetObjectClass(self);
	jfieldID fid = env->GetFieldID(klass, "mNativePtr", "J");
	assert(fid != NULL);
    jlong ptr = env->GetLongField(self, fid);
    env->DeleteLocalRef(klass);
    return reinterpret_cast<SqliteDB*>(ptr);
}

jint JNICALL Java_pi_sqlite_SQLite_open__Ljava_lang_String_2
(JNIEnv * env, jobject self, jstring jstrFilename)
{
	jclass klass = env->GetObjectClass(self);

	jfieldID fid = env->GetFieldID(klass, "mNativePtr", "J");
	assert(fid != NULL);

	SqliteDB* db = reinterpret_cast<SqliteDB*>(env->GetLongField(self, fid));
	if (db != NULL) {
		db->Release();
	}

	db = new SqliteDB();
	db->Retain();

	const char* filename = env->GetStringUTFChars(jstrFilename, NULL);

	int ret = sqlite3_open(filename, &db->conn);
	env->ReleaseStringUTFChars(jstrFilename, filename);

	env->SetLongField(self, fid, reinterpret_cast<jlong>(db));
	env->DeleteLocalRef(klass);

	return ret;
}

jint JNICALL Java_pi_sqlite_SQLite_open__Ljava_lang_String_2ILjava_lang_String_2
(JNIEnv * env, jobject self, jstring jstrFilename, jint flags, jstring jstrVFS)
{
	jclass klass = env->GetObjectClass(self);

	jfieldID fid = env->GetFieldID(klass, "mNativePtr", "J");
	assert(fid != NULL);

	SqliteDB* db = reinterpret_cast<SqliteDB*>(env->GetLongField(self, fid));
	if (db != NULL) {
		db->Release();
	}

	db = new SqliteDB();
	db->Retain();

	const char* filename = env->GetStringUTFChars(jstrFilename, NULL);
	const char* vfs = NULL;
	if (jstrVFS != NULL)
	{
		vfs = env->GetStringUTFChars(jstrVFS, NULL);
	}

	int ret = sqlite3_open_v2(filename, &db->conn, flags, vfs);

	env->ReleaseStringUTFChars(jstrFilename, filename);

	if (jstrVFS != NULL)
	{
		env->ReleaseStringUTFChars(jstrVFS, vfs);
	}

	env->SetLongField(self, fid, reinterpret_cast<jlong>(db));
	env->DeleteLocalRef(klass);

	return ret;
}

void JNICALL Java_pi_sqlite_SQLite_close
(JNIEnv * env, jobject self)
{
	jclass klass = env->GetObjectClass(self);

	jfieldID fid = env->GetFieldID(klass, "mNativePtr", "J");
	assert(fid != NULL);

	SqliteDB* db = reinterpret_cast<SqliteDB*>(env->GetLongField(self, fid));
	if (db != NULL)
	{
		db->Release();
		db = NULL;
	}

	env->SetLongField(self, fid, 0);
	env->DeleteLocalRef(klass);
}

jobject JNICALL Java_pi_sqlite_SQLite_createStatement
(JNIEnv *env, jobject self)
{
	SqliteDB* db = GetDB(env, self);
	if (db == NULL)
	{
		return NULL;
	}

	jclass stmtClass = env->FindClass("pi/sqlite/SQLiteStatement");
	assert(stmtClass != NULL);

	jmethodID initMid = env->GetMethodID(stmtClass, "<init>", "(J)V");
	assert(initMid != NULL);

	jobject javaStmt = env->NewObject(stmtClass, initMid, reinterpret_cast<jlong>(db));
	env->DeleteLocalRef(stmtClass);

	return javaStmt;
}

jint JNICALL Java_pi_sqlite_SQLite_getErrorCode
(JNIEnv *env, jobject self)
{
	SqliteDB* db = GetDB(env, self);
	if (db == NULL) 
	{
		return -1;
	}
	return sqlite3_errcode(db->conn);
}

jint JNICALL Java_pi_sqlite_SQLite_getExtendedErrorCode
(JNIEnv *env, jobject self)
{
	SqliteDB* db = GetDB(env, self);
	if (db == NULL)
	{
		return -1;
	}
	return sqlite3_extended_errcode(db->conn);
}

jstring JNICALL Java_pi_sqlite_SQLite_getErrorString
(JNIEnv *env, jobject self, jint errCode)
{
	const char* str = sqlite3_errstr(errCode);
	return env->NewStringUTF(str);
}

jstring JNICALL Java_pi_sqlite_SQLite_getErrorMessage
(JNIEnv *env, jobject self)
{
	SqliteDB* db = GetDB(env, self);
	if (db == NULL)
	{
		return NULL;
	}
	const char* str = sqlite3_errmsg(db->conn);
	return env->NewStringUTF(str);
}

























