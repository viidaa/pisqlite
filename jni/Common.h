#include <stdio.h>
#include "sqlite3.h"

class SqliteDB
{
private:
	int mRefCount;

public:
	sqlite3* conn;

	SqliteDB():
	conn(NULL),
	mRefCount(0)
	{
	}

	~SqliteDB()
	{
		if (conn != NULL) 
		{
			sqlite3_close_v2(conn);
			conn = NULL;
		}
	}

	void Retain()
	{
		__sync_fetch_and_add(&mRefCount, 1);
	}

	void Release()
	{
		if (__sync_fetch_and_sub(&mRefCount, 1) == 1)
		{
			delete this;
		}
	}
};