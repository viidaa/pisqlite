APP_PLATFORM 			:= android-14
APP_STL           		:= gnustl_static
APP_ABI 	  			:= armeabi-v7a
APP_PROJECT_PATH  		:= $(LOCAL_PATH)
APP_MODULES 	  		:= pisqlite
#NDK_TOOLCHAIN_VERSION 	:= 4.6
APP_CPPFLAGS 			:= -frtti -fexceptions
APP_CFLAGS 				:= -Wno-psabi