package pi.sqlite;

public class SQLiteStatement {

	private long mNativePtr;
	
	private long mDbNativePtr;
	
	private SQLiteStatement(long dbNativePtr) {
		mDbNativePtr = dbNativePtr;
	}
	
	public void finalize() {
		close();
	}
	
	synchronized native public int prepare(String sql);
	
	synchronized native public void reset();
	
	synchronized native public void close();
	
	synchronized native public int step();
	
	synchronized native public int bind(int index, byte[] blob, int offset, int size);
	
	synchronized native public int bind(int index, double v);
	
	synchronized native public int bind(int index, int v);
	
	synchronized native public int bind(int index, long v);
	
	synchronized native public int bind(int index, String v);
	
	synchronized native public int bindNull(int index);
	
	synchronized native public int bindZeroBlob(int index, int size);
	
	synchronized native public int getColumnCount();
	
	synchronized native public String getColumnName(int index);
	
	synchronized native public int getColumnType(int index);
	
	synchronized native public byte[] getColumnBlob(int index);
	
	synchronized native public double getColumnDouble(int index);
	
	synchronized native public int getColumnInt(int index);
	
	synchronized native public long getColumnLong(int index);
	
	synchronized native public String getColumnText(int index);
	
	synchronized public int getAffectedRows() {
		return getChanges();
	}
	
	synchronized native public int getChanges();
	
	synchronized native public int getInsertId();
}

















