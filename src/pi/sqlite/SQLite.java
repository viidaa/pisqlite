package pi.sqlite;

public class SQLite {
	
	static {
		System.loadLibrary("pisqlite");
	}

	public static final int OPEN_READONLY         = 0x00000001; /* Ok for sqlite3_open_v2() */
	public static final int OPEN_READWRITE        = 0x00000002; /* Ok for sqlite3_open_v2() */
	public static final int OPEN_CREATE           = 0x00000004; /* Ok for sqlite3_open_v2() */
	public static final int OPEN_DELETEONCLOSE    = 0x00000008; /* VFS only */
	public static final int OPEN_EXCLUSIVE        = 0x00000010; /* VFS only */
	public static final int OPEN_AUTOPROXY        = 0x00000020; /* VFS only */
	public static final int OPEN_URI              = 0x00000040; /* Ok for sqlite3_open_v2() */
	public static final int OPEN_MEMORY           = 0x00000080; /* Ok for sqlite3_open_v2() */
	public static final int OPEN_MAIN_DB          = 0x00000100; /* VFS only */
	public static final int OPEN_TEMP_DB          = 0x00000200; /* VFS only */
	public static final int OPEN_TRANSIENT_DB     = 0x00000400; /* VFS only */
	public static final int OPEN_MAIN_JOURNAL     = 0x00000800; /* VFS only */
	public static final int OPEN_TEMP_JOURNAL     = 0x00001000; /* VFS only */
	public static final int OPEN_SUBJOURNAL       = 0x00002000; /* VFS only */
	public static final int OPEN_MASTER_JOURNAL   = 0x00004000; /* VFS only */
	public static final int OPEN_NOMUTEX          = 0x00008000; /* Ok for sqlite3_open_v2() */
	public static final int OPEN_FULLMUTEX        = 0x00010000; /* Ok for sqlite3_open_v2() */
	public static final int OPEN_SHAREDCACHE      = 0x00020000; /* Ok for sqlite3_open_v2() */
	public static final int OPEN_PRIVATECACHE     = 0x00040000; /* Ok for sqlite3_open_v2() */
	public static final int OPEN_WAL              = 0x00080000; /* VFS only */
	
	public static final int OK          = 0;   /* Successful result */
	/* beginning-of-error-codes */
	public static final int ERROR       = 1;   /* SQL error or missing database */
	public static final int INTERNAL    = 2;   /* Internal logic error in SQLite */
	public static final int PERM        = 3;   /* Access permission denied */
	public static final int ABORT       = 4;   /* Callback routine requested an abort */
	public static final int BUSY        = 5;   /* The database file is locked */
	public static final int LOCKED      = 6;   /* A table in the database is locked */
	public static final int NOMEM       = 7;   /* A malloc() failed */
	public static final int READONLY    = 8;   /* Attempt to write a readonly database */
	public static final int INTERRUPT   = 9;   /* Operation terminated by sqlite3_interrupt()*/
	public static final int IOERR       = 10;   /* Some kind of disk I/O error occurred */
	public static final int CORRUPT     = 11;   /* The database disk image is malformed */
	public static final int NOTFOUND    = 12;   /* Unknown opcode in sqlite3_file_control() */
	public static final int FULL        = 13;   /* Insertion failed because database is full */
	public static final int CANTOPEN    = 14;   /* Unable to open the database file */
	public static final int PROTOCOL    = 15;   /* Database lock protocol error */
	public static final int EMPTY       = 16;   /* Database is empty */
	public static final int SCHEMA      = 17;   /* The database schema changed */
	public static final int TOOBIG      = 18;   /* String or BLOB exceeds size limit */
	public static final int CONSTRAINT  = 19;   /* Abort due to constraint violation */
	public static final int MISMATCH    = 20;   /* Data type mismatch */
	public static final int MISUSE      = 21;   /* Library used incorrectly */
	public static final int NOLFS       = 22;   /* Uses OS features not supported on host */
	public static final int AUTH        = 23;   /* Authorization denied */
	public static final int FORMAT      = 24;   /* Auxiliary database format error */
	public static final int RANGE       = 25;   /* 2nd parameter to sqlite3_bind out of range */
	public static final int NOTADB      = 26;   /* File opened that is not a database file */
	public static final int NOTICE      = 27;   /* Notifications from sqlite3_log() */
	public static final int WARNING     = 28;   /* Warnings from sqlite3_log() */
	public static final int ROW         = 100;  /* sqlite3_step() has another row ready */
	public static final int DONE        = 101;  /* sqlite3_step() has finished executing */
	
	
	
	
	
	public static final int IOERR_READ              = (IOERR | (1<<8));
	public static final int IOERR_SHORT_READ        = (IOERR | (2<<8));
	public static final int IOERR_WRITE             = (IOERR | (3<<8));
	public static final int IOERR_FSYNC             = (IOERR | (4<<8));
	public static final int IOERR_DIR_FSYNC         = (IOERR | (5<<8));
	public static final int IOERR_TRUNCATE          = (IOERR | (6<<8));
	public static final int IOERR_FSTAT             = (IOERR | (7<<8));
	public static final int IOERR_UNLOCK            = (IOERR | (8<<8));
	public static final int IOERR_RDLOCK            = (IOERR | (9<<8));
	public static final int IOERR_DELETE            = (IOERR | (10<<8));
	public static final int IOERR_BLOCKED           = (IOERR | (11<<8));
	public static final int IOERR_NOMEM             = (IOERR | (12<<8));
	public static final int IOERR_ACCESS            = (IOERR | (13<<8));
	public static final int IOERR_CHECKRESERVEDLOCK = (IOERR | (14<<8));
	public static final int IOERR_LOCK              = (IOERR | (15<<8));
	public static final int IOERR_CLOSE             = (IOERR | (16<<8));
	public static final int IOERR_DIR_CLOSE         = (IOERR | (17<<8));
	public static final int IOERR_SHMOPEN           = (IOERR | (18<<8));
	public static final int IOERR_SHMSIZE           = (IOERR | (19<<8));
	public static final int IOERR_SHMLOCK           = (IOERR | (20<<8));
	public static final int IOERR_SHMMAP            = (IOERR | (21<<8));
	public static final int IOERR_SEEK              = (IOERR | (22<<8));
	public static final int IOERR_DELETE_NOENT      = (IOERR | (23<<8));
	public static final int IOERR_MMAP              = (IOERR | (24<<8));
	public static final int IOERR_GETTEMPPATH       = (IOERR | (25<<8));
	public static final int IOERR_CONVPATH          = (IOERR | (26<<8));
	public static final int LOCKED_SHAREDCACHE      = (LOCKED |  (1<<8));
	public static final int BUSY_RECOVERY           = (BUSY   |  (1<<8));
	public static final int BUSY_SNAPSHOT           = (BUSY   |  (2<<8));
	public static final int CANTOPEN_NOTEMPDIR      = (CANTOPEN | (1<<8));
	public static final int CANTOPEN_ISDIR          = (CANTOPEN | (2<<8));
	public static final int CANTOPEN_FULLPATH       = (CANTOPEN | (3<<8));
	public static final int CANTOPEN_CONVPATH       = (CANTOPEN | (4<<8));
	public static final int CORRUPT_VTAB            = (CORRUPT | (1<<8));
	public static final int READONLY_RECOVERY       = (READONLY | (1<<8));
	public static final int READONLY_CANTLOCK       = (READONLY | (2<<8));
	public static final int READONLY_ROLLBACK       = (READONLY | (3<<8));
	public static final int READONLY_DBMOVED        = (READONLY | (4<<8));
	public static final int ABORT_ROLLBACK          = (ABORT | (2<<8));
	public static final int CONSTRAINT_CHECK        = (CONSTRAINT | (1<<8));
	public static final int CONSTRAINT_COMMITHOOK   = (CONSTRAINT | (2<<8));
	public static final int CONSTRAINT_FOREIGNKEY   = (CONSTRAINT | (3<<8));
	public static final int CONSTRAINT_FUNCTION     = (CONSTRAINT | (4<<8));
	public static final int CONSTRAINT_NOTNULL      = (CONSTRAINT | (5<<8));
	public static final int CONSTRAINT_PRIMARYKEY   = (CONSTRAINT | (6<<8));
	public static final int CONSTRAINT_TRIGGER      = (CONSTRAINT | (7<<8));
	public static final int CONSTRAINT_UNIQUE       = (CONSTRAINT | (8<<8));
	public static final int CONSTRAINT_VTAB         = (CONSTRAINT | (9<<8));
	public static final int CONSTRAINT_ROWID        = (CONSTRAINT |(10<<8));
	public static final int NOTICE_RECOVER_WAL      = (NOTICE | (1<<8));
	public static final int NOTICE_RECOVER_ROLLBACK = (NOTICE | (2<<8));
	public static final int WARNING_AUTOINDEX       = (WARNING | (1<<8));
	
	//===========================================================================
	
	private long mNativePtr;
	
	public SQLite() {
	}
	
	public void finalize() {
		close();
	}
	
	synchronized native public int open(String filename);
	
	synchronized native public int open(String filename, int flags, String vfs);
	
	synchronized native public void close();
	
	synchronized native public SQLiteStatement createStatement();
	
	synchronized native public int getErrorCode();
	
	synchronized native public int getExtendedErrorCode();
	
	synchronized native public String getErrorString(int errCode);
	
	synchronized native public String getErrorMessage();
}




