package pi.sqlite;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Database {

	private SQLite mDB;
	
	private File mDbFile;
	
	private List<Table> mTables = new ArrayList<Table>();
	
	public Database(File dbFile) {
		mDbFile = dbFile;
	}
	
	public void finalize() {
		close();
	}
	
	public void addTable(Table table) {
		assert(table != null);
		mTables.add(table);
	}
	
	public boolean open() {		
		File dir = mDbFile.getParentFile();
		if (!dir.exists() && !dir.mkdirs()) {
			return false;
		}
		
		SQLite db = new SQLite();
		int ret = db.open(mDbFile.getAbsolutePath(), SQLite.OPEN_READWRITE|SQLite.OPEN_CREATE|SQLite.OPEN_SHAREDCACHE, null);
		if (ret != SQLite.OK) {
			db.close();
			return false;
		}
		
		if (!initTables(db)) {
			return false;
		}
		
		mDB = db;
		return true;
	}
	
	public void close() {
		if (mDB != null) {
			mDB.close();
			mDB = null;
		}
	}
	
	public SQLite getHandle() {
		return mDB;
	}
	
	public SQLiteStatement compile(String sql) {
		SQLiteStatement stmt = mDB.createStatement();
		if (stmt.prepare(sql) == SQLite.OK) {
			return stmt;
		} else {
			stmt.close();
			return null;
		}
	}
	
	private boolean initTables(SQLite db) {
		for (Table t : mTables) {
			if (!t.init(db)) {
				return false;
			}
		}
		return true;
	}
}





















